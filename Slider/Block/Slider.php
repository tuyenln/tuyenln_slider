<?php
namespace Tuyenln\Slider\Block;
class Slider extends \Magento\Framework\View\Element\Template {
	protected $_scopeConfig;
	public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Framework\View\Element\Template\Context $context,
		array $data = []
	) {
		parent::__construct($context,$data);
		$this->_scopeConfig = $scopeConfig;
	}

  public function getSliderImages(){
	  $scope=\Magento\Store\Model\ScopeInterface::SCOPE_STORE;
	  $basepath='slider/parameters/';
	  $folderName = \Tuyenln\Slider\Model\Saveimage::UPLOAD_DIR;
	  $urlBuilder=$this->_urlBuilder;
	  return array(
				 array(
					'slider_image'=>$urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $folderName.'/'.$this->_scopeConfig->getValue($basepath.'slider_image_1',$scope),
				 ),
				 array(
					'slider_image'=>$urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $folderName.'/'.$this->_scopeConfig->getValue($basepath.'slider_image_2',$scope),
				 ),
				 array(
					'slider_image'=>$urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $folderName.'/'.$this->_scopeConfig->getValue($basepath.'slider_image_3',$scope),
				 ),
				 array(
					'slider_image'=>$urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $folderName.'/'.$this->_scopeConfig->getValue($basepath.'slider_image_4',$scope),
				 ),
				 array(
					'slider_image'=>$urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $folderName.'/'.$this->_scopeConfig->getValue($basepath.'slider_image_5',$scope),
				 )
			);
  }
	public function getSliderConfig(){
		$scope=\Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		$basepath='slider/parameters/';
		return array(
					'autoheight'=> $this->_scopeConfig->getValue($basepath.'autoheight',$scope),
					'auto'=> $this->_scopeConfig->getValue($basepath.'auto',$scope),
					'slide_speed'=> $this->_scopeConfig->getValue($basepath.'slide_speed',$scope),
					'mode'=> $this->_scopeConfig->getValue($basepath.'mode',$scope)
				);
	}
}
?>
