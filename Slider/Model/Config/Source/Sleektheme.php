<?php
namespace Tuyenln\Slider\Model\Config\Source;
use Magento\Framework\Data\OptionSourceInterface;
class Sleektheme implements OptionSourceInterface {
    public function toOptionArray() {
            return [
                ['value' => 'vertical', 'label' => __('Vertical')],
                ['value' => 'fade', 'label' => __('Fade')],
                ['value' => 'horizontal', 'label' => __('Horizontal')],
            ];
    }
}
?>
